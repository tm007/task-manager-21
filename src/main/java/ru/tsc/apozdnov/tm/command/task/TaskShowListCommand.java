package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskShowListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Show task-list";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("**** TASK LIST ****");
        System.out.println("**** ENTER SORT ****");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = serviceLocator.getTaskService().findAll(getUserId(), sort);
        renderTasks(tasks);
    }

}
