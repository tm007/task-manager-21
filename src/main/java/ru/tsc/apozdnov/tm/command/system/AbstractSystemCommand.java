package ru.tsc.apozdnov.tm.command.system;

import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import javax.management.relation.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public RoleType[] getRoleType() {
        return new RoleType[0];
    }

}
