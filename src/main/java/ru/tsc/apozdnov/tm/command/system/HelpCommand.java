package ru.tsc.apozdnov.tm.command.system;

import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Display all commands";
    }

    @Override
    public RoleType[] getRoleType() {
        return RoleType.values();
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommand();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
