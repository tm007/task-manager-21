package ru.tsc.apozdnov.tm.command.system;

import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show commands list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommand();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
