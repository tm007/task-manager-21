package ru.tsc.apozdnov.tm.command;

import ru.tsc.apozdnov.tm.api.model.ICommand;
import ru.tsc.apozdnov.tm.api.service.IAuthService;
import ru.tsc.apozdnov.tm.api.service.IServiceLocator;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import javax.management.relation.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public abstract RoleType[] getRoleType();

    public String getArgument() {
        return null;
    }

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getIAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getIAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
