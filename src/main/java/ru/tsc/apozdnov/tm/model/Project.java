package ru.tsc.apozdnov.tm.model;

import ru.tsc.apozdnov.tm.api.model.IWBS;
import ru.tsc.apozdnov.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String descriprion = "";

    private Status status = Status.NOT_STARTED;

    private Date dateCreated = new Date();

    private Date dateBegin = new Date();

    private Date dateEnd = new Date();

    public Project() {
    }

    public Project(final String name, final String descriprion, final Status status, final Date dateBegin) {
        this.name = name;
        this.descriprion = descriprion;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(final Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(final Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriprion() {
        return descriprion;
    }

    public void setDescriprion(String descriprion) {
        this.descriprion = descriprion;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "NAME" + ": " + name + " |" +
                "DESCRIPTION: " + ": " + descriprion + " | " +
                "STATUS:" + " " + getStatus().getDisplayName() + "|" +
                "DATE BEGIN:" + " " + dateBegin + "\n";
    }

}
