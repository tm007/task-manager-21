package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;

import ru.tsc.apozdnov.tm.api.service.IProjectService;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.field.NameEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project updateByIndex(final String userId, Integer index, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescriprion(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateById(final String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = Optional.ofNullable(findOneById(id)).orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescriprion(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = Optional.ofNullable(findOneById(userId, id)).orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final Project project = Optional.ofNullable(create(userId, name, description)).orElseThrow(ProjectNotFoundException::new);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project add(final String userId, Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        return repository.add(project);
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear();
    }

}
