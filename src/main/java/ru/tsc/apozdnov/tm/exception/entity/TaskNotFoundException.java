package ru.tsc.apozdnov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Fault!!! Task not found!!!");
    }

}
