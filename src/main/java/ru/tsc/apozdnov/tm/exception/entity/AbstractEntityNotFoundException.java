package ru.tsc.apozdnov.tm.exception.entity;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
