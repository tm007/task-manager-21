package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

}
