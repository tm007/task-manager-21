package ru.tsc.apozdnov.tm.api.service;

public interface ILoggerService {

    void info(String messsage);

    void command(String messsage);

    void error(Exception ex);

    void debug(String message);

}
