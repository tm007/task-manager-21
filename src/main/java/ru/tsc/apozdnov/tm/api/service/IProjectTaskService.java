package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

}
